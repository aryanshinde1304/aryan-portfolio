import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { HiOutlineMenuAlt3 } from 'react-icons/hi';

const MenuItems = ({ children, url }) => {
    return (
        <RouterLink
         
         to={url}
         className="flex space-x-4 md:mt-0 sm:mt-4 lg:mt-0 text-sm tracking-wide text-slate-600 text-transform: uppercase mr-5 block hover:text-slate-300"
        >
        {children}
        </RouterLink>
    )
}

 const Header = () => {
    const [show, setShow] = useState(false);
  return (
    <>
    {/* <h1 className="text-3xl font-bold underline">
    Hello world!
  </h1>

  <div className="flex gap-4">
    <div>Hello</div>
    <div>World</div>
    <div>Demon</div>
  </div> */}

 
        <div className='flex text-center justify-between py-6 px-6 w-full top-0 backdrop-filter backdrop-blur-lg bg-transparent position: sticky z-2  bg-opacity-30'>

        <div className='flex text-center mr-5 font-bold tracking-wide'>
        <h1 className='text-transform: lowercase font-bold no-underline hover:underline'>
            aryan shinde.
        </h1>
        </div>

        <div className="block md:hidden" >
            <div className='text-black w-6 h-6' onClick={() => 
                    
                setShow(!show)}>
                <HiOutlineMenuAlt3 />
            </div>
            <title>Menu</title>
        </div>
        
        <div className={`items-center ${show ? "flex" : "hidden"} md:flex items-center w-full md:w-auto flex-col md:flex-row`}>
            <MenuItems>
                <div className="text-center">
                    Home
                </div>
            </MenuItems>
            <MenuItems>
                <div className="text-center">
                    About
                </div>
            </MenuItems>
            <MenuItems>
                <div className="text-center">
                    Project
                </div>
            </MenuItems>    
            <MenuItems>
                <div className="text-center">
                Contact me
                </div>
            </MenuItems>
        </div>
        </div>
    

  </>
  )
}

export default Header;