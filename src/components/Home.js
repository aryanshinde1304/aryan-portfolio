import React from "react";

const Home = () => {
  return (
    <div className="flex h-screen bg-slate-300 max-h-full">
      <div className="flex m-auto flex-col items-center justify-center content-center mx-[25px] lg:mx-[100px] ">
        <p className="text-5xl font-extrabold text-center">I'm Aryan Shinde</p>
        <p className="flex text-lg font-normal mt-4 text-center">As a full-stack developer, I have more than one year of experience working with the MERN stack, which consists of React, Node.js, Express, and MongoDB. I have a background of developing and maintaining web applications from scratch, and I'm passionate about creating cutting-edge, usable solutions.</p>
        <button type="button" className="mt-4 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-md px-10 py-2 text-center mr-2 mb-2 dark:border-gray-600 dark:text-black-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800">Projects</button>
      </div>
    </div>
  );
};

export default Home;
